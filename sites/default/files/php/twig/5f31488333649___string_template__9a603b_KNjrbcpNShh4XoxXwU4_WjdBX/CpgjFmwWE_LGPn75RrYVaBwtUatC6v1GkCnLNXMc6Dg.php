<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__9a603b90578e3446dfb61129696d4e68aaffe0e487cc1a2baf96732ae8fe74f8 */
class __TwigTemplate_5d0b66dc98cdce096f15dbfde84a440be29aff31fc16f219eb41e8bb31959877 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 7, "trans" => 9];
        $filters = ["escape" => 13];
        $functions = ["url" => 7];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'trans'],
                ['escape'],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 6
        echo "
";
        // line 7
        $context["block_admin_page"] = $this->getAttribute($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("block.admin_display"), "#markup", [], "array");
        // line 8
        echo "
";
        // line 9
        echo t("<p>The Block Example provides three sample blocks which demonstrate the various
  block APIs. To experiment with the blocks, enable and configure them on
  <a href=\"@block_admin_page\">the block admin page</a>.</p>", array("@block_admin_page" =>         // line 13
($context["block_admin_page"] ?? null), ));
    }

    public function getTemplateName()
    {
        return "__string_template__9a603b90578e3446dfb61129696d4e68aaffe0e487cc1a2baf96732ae8fe74f8";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 13,  63 => 9,  60 => 8,  58 => 7,  55 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__9a603b90578e3446dfb61129696d4e68aaffe0e487cc1a2baf96732ae8fe74f8", "");
    }
}
