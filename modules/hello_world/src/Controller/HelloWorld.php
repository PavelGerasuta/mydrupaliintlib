<?php


/**
 * @file
 * Contains \Drupal\hello_world\Controller\.
 */

namespace Drupal\hello_world\Controller;

/**
 * Class FirstPage
 * Provides rout for our custom module.
 * @package Drupal\hello_world\Controller
 */

class HelloWorld {


  /**
   * Display simple page.
   */
  public function content() {
    return array(
      '#markup' => 'Hello, world',
    );


  }

  public function UID(){
    $user = \Drupal::currentUser()
      ->id();
  /*  $n = \Drupal::currentUser()
      ->getDisplayName();
    $t = \Drupal::currentUser()->getTimeZone();
    $t1 = \Drupal::time()->getCurrentTime();
  */
    /**
     *{@inheritdoc}
     */
    return [

      '#markup' => 'Привет пользователь под номером ' . $user . "<br>" . date(' H:i:s')
    ];
  }

}
