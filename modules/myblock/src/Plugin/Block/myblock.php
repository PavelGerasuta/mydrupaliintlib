<?php

namespace Drupal\myblock\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'IUDBlock'
 *
 * @Block(
 *     id = "my_block",
 *     admin_label = @Translation("myblock"),
 *     category = @Translation("myblock"),
 *   )
 */

class myblock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $user = \Drupal::currentUser()->id();
    $d = getdate();
    return [
      '#markup' => $this->t("Привет, пользоваетль под номером " . $user. " Сегодня: " . $d[mday]. ". " . $d[mon].". ".$d[year] .
        " Время: ".$d[hours]." :".$d[minutes]." :".$d[seconds].
        " День недели: ".$d[weekday]." Месяц: ".$d[month]) ,
      '#cache' => ['contexts' => ['user']],
    ];

  } 

  function hook_cron() {


    $expires = \Drupal::state()
      ->get('myblock.last_check', 0);
    \Drupal::database()
      ->delete('myblock_table')
      ->condition('expires', $expires, '>=')
      ->execute();
    \Drupal::state()
      ->set('myblock.last_check', REQUEST_TIME);


    $queue = \Drupal::queue('aggregator_feeds');
    $ids = \Drupal::entityTypeManager()

      ->getStorage('aggregator_feed')

      ->getFeedIdsToRefresh();

    foreach (Feed::loadMultiple($ids) as $feed) {

      if ($queue
        ->createItem($feed)) {

        // Add timestamp to avoid queueing item more than once.
        $feed
          ->setQueuedTime(REQUEST_TIME);
        $feed
          ->save();
      }
    }
    $ids = \Drupal::entityQuery('aggregator_feed')
      ->condition('queued', REQUEST_TIME - 3600 * 6, '<')
      ->execute();
    if ($ids) {
      $feeds = Feed::loadMultiple($ids);
      foreach ($feeds as $feed) {
        $feed
          ->setQueuedTime(0);
        $feed
          ->save();
      }
    }
  }


  public static function currentUser() {
    return static::getContainer()
      ->get('current_user');
  }



}
