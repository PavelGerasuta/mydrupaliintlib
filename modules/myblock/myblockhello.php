<?php

/**
 * @file
 * Main file for hooks and custom functions.
 */

use Drupal\Core\StringTranslation\TranslatableMarkup;


/**
 * Implements hook_form_FORM_ID_alter().
 */

function myblock_form_user_login_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  $form['welcome'] = [
    '#markup' => '<h2>Hello world!</h2>',

  ];

  $form['name']['#attributes']['placeholder'] = 'Enter your username here!';
}

/**
 * Implements hook_user_login().
 */
function myblock_user_login($account) {
  $message = new TranslatableMarkup('Nice to see you again, <strong>@username</strong>!', [
    '@username' => $account->getDisplayName(),
  ]);
  \Drupal::messenger()->addMessage($message);
}

/**
 * Implements hook_user_format_name_alter().
 */

function myblock_user_format_name_alter(&$name, $account) {
  $name .= ' (' . $account->id() . ')';
}
