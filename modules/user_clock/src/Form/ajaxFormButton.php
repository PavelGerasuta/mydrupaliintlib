<?php

namespace Drupal\user_clock\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Custom simple form class.
 */
class ajaxFormButton extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_clock_ajax_form_button';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {


    $form['massage'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result_message">Время '. date('H:i:s').'</div><br>',
    ];


    $form['actions'] = [
      '#type' => 'button',
      '#value' => $this->t('Refresh time'),
      '#ajax' => [
        'callback' => '::setMessage',
      ]
    ];

    return $form;
  }

  public function setMessage(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();


    $response->addCommand(
      new HtmlCommand(
        '.result_message',
        '<div class="my_top_message">' . $this->t('Время ' . date(' H:i:s'))

      )
    );



    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
